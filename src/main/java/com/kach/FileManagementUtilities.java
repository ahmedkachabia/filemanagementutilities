package com.kach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileManagementUtilities {

	public static void main(String[] args) {
		SpringApplication.run(FileManagementUtilities.class, args);
	}

}
