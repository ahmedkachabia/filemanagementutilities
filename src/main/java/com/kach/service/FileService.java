package com.kach.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FileService {

	@Value("${file.upload-location}")
	private String uploadLocation;

	public String moveFile(String sourceFile, String destinationFolder) throws IOException {
		Path fileToMovePath = Paths.get(sourceFile);
		String fileName = fileToMovePath.getFileName().toString();
		Path targetPath = Paths.get(destinationFolder + "/" + fileName);
		return Files.move(fileToMovePath, targetPath).getFileName().toString();
	}

	public String renameFile(String sourceFile, String newFileName) throws IOException {
		Path source = Paths.get(sourceFile);
		return Files.move(source, source.resolveSibling(newFileName)).toString();
	}

	public void deleteFile(String fileToDelete) throws IOException {

		Path fileToDeletePath = Paths.get(fileToDelete);
		Files.delete(fileToDeletePath);
	}

	public String uploadFile(String sourceFile, String destinationFolder) throws IOException {
		Path fileToMovePath = Paths.get(sourceFile);
		String fileName = fileToMovePath.getFileName().toString();
		Path targetPath = Paths.get(destinationFolder + "/" + fileName);
		return Files.move(fileToMovePath, targetPath).getFileName().toString();
	}

//    private Path writeFileToFs(InputStream is, String fileName) {
//        ReadableByteChannel in = null;
//        FileChannel out = null;
//        ByteBuffer buf = ByteBuffer.allocate(BUFF_CAPACITY);
//        Path filePath = null;
//        try {
//            filePath = Paths.get(fileName);
//
//            in = Channels.newChannel(is);
//
//            Files.deleteIfExists(filePath);
//            Path path = Files.createFile(filePath);
//            out = new FileOutputStream(path.toFile()).getChannel();
//
//            buf.clear();
//            while (in.read(buf) > -1) {
//                buf.flip();
//                out.write(buf);
//                buf.clear();
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(StorageService.class.getName()).log(Level.SEVERE, null, ex);
//            throw new RuntimeException(ex);
//        } finally {
//            try {
//                if (in != null) {
//                    in.close();
//                }
//                if (out != null) {
//                    out.close();
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(StorageService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        return filePath;
//    }

}
