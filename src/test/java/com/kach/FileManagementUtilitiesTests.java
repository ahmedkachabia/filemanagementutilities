package com.kach;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.kach.service.FileService;

@SpringBootTest
class FileManagementUtilitiesTests {

	@Autowired
	FileService fileService;
	
	@Test
	void contextLoads() {
	}
	
	@Test
	void testFileMove() throws IOException {
		fileService.moveFile("C:\\Users\\Ahmed\\Downloads\\ideaIC-2022.2.exe", "C:\\Users\\Ahmed\\Downloads\\sts-4.15.3.RELEASE");
	}
	
	@Test
	void testFileDelete() throws IOException {
		fileService.deleteFile("C:\\Users\\Ahmed\\Downloads\\ideaIC-2022.2.exe");
	}
	
	@Test
	void testRenameFile() throws IOException {
		fileService.renameFile("C:\\Users\\Ahmed\\Downloads\\ASANA.pdf", "AS.pdf");
	}

}
